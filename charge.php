<?php
require_once("stripe-php-master/init.php");

// Set your secret key: remember to change this to your live secret key in production
// See your keys here: https://dashboard.stripe.com/account/apikeys
\Stripe\Stripe::setApiKey('sk_test_kvdnw9cqBtkDGpXTSGi1CQqi00hJP1IYmm');

// Token is created using Checkout or Elements!
// Get the payment token ID submitted by the form:
$token = $_POST['stripeToken'];
$amount = $_POST['amount'];
try{$charge = \Stripe\Charge::create([
    'amount' => $amount,
    'currency' => 'usd',
    'description' => 'Example charge',
    'source' => $token,
]);} catch(\Stripe\Error\Card $e) {
  // Since it's a decline, \Stripe\Error\Card will be caught
  $body = $e->getJsonBody();
  $err  = $body['error'];
  $data = [ 'status' => $e->getHttpStatus(), 'type' => $err['type'], 'code' =>  $err['code'],  'message' =>  $err['message'] ];;
  header('Content-Type: application/json');
  echo json_encode($data);

  http_response_code($e->getHttpStatus());

  print('Status is:' . $e->getHttpStatus() . "\n");
  print('Type is:' . $err['type'] . "\n");
  print('Code is:' . $err['code'] . "\n");
  // param is '' in this case
  print('Param is:' . $err['param'] . "\n");
  print('Message is:' . $err['message'] . "\n");
} catch (\Stripe\Error\RateLimit $e) {
    http_response_code($e->getHttpStatus());
  // Too many requests made to the API too quickly
} catch (\Stripe\Error\InvalidRequest $e) {
    http_response_code($e->getHttpStatus());
  // Invalid parameters were supplied to Stripe's API
} catch (\Stripe\Error\Authentication $e) {
    http_response_code($e->getHttpStatus());
  // Authentication with Stripe's API failed
  // (maybe you changed API keys recently)
} catch (\Stripe\Error\ApiConnection $e) {
    http_response_code($e->getHttpStatus());
  // Network communication with Stripe failed
} catch (\Stripe\Error\Base $e) {
    http_response_code($e->getHttpStatus());
  // Display a very generic error to the user, and maybe send
  // yourself an email
} catch (Exception $e) {
    http_response_code($e->getHttpStatus());
  // Something else happened, completely unrelated to Stripe
}
// header("Location: index.html");
// exit();

?>
var donationAmount = 0;

$("#amountMeModal").on("shown.bs.modal", function() {
  $("#donationAmountButton").prop("disabled", true);
});

$("#donationAmount").change(function() {
  if ($("#donationAmountButton").prop("disabled")) {
    $("#donationAmountButton").prop("disabled", false);
  }
});

$("#donationAmountButton").click(function() {
  donationAmount = $("#donationAmount").val() + 00;
});

$("#donationAmount20").click(function() {
  donationAmount = 2000;
});

$("#donationAmount50").click(function() {
  donationAmount = 5000;
});

$("#donationAmount100").click(function() {
  donationAmount = 10000;
});

$("#paypalMethodButton").click(function() {
  var paypalDonationAmount = +donationAmount / 100;
  $("#paypalAmount").val(paypalDonationAmount);
});

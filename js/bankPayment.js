//bank transfer

document
  .getElementById("bankLocationSubmitButton")
  .addEventListener("click", function() {
    var bankLocationform = document.getElementById("bankLocationForm");

    let myheader = new Headers();
    myheader.append("Authorization", "Bearer dd82bfde08177bbeba3d76dee42c542c");
    myheader.append(
      "Content-Type",
      "application/x-www-form-urlencoded; charset=UTF-8"
    );

    var config = {
      headers: {
        Authorization: "bearer dd82bfde08177bbeba3d76dee42c542c",
        "Content-Type": "application/x-www-form-urlencoded"
      }
    };

    cdata = {
      email: document.getElementById("email").value,
      full_name: document.getElementById("full_name").value,
      address_line1: document.getElementById("autocomplete").value,
      address_line2: document.getElementById("address2").value,
      address_city: document.getElementById("locality").value,
      address_state: document.getElementById("administrative_area_level_1")
        .value,
      address_postal_code: document.getElementById("postal_code").value,
      address_country: document.getElementById("country").value
    };

    const searchParams = Object.keys(cdata)
      .map(key => {
        return encodeURIComponent(key) + "=" + encodeURIComponent(cdata[key]);
      })
      .join("&");

    fetch("https://dataco.bubbleapps.io/api/1.1/wf/s_bank", {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
        Authorization: "bearer dd82bfde08177bbeba3d76dee42c542c"
      },
      body: searchParams
    }).then(response => {
      bankLocationform.reset();
      if (response.ok) {
        response.json().then(json => {
          $("#bankModal").modal("hide");
          $("#bank-account-number").val(json.response.acc_num);
          $("#bank-routing-number").val(json.response.routing_num);
          $("#bank-swift-code").val(json.response.swift_code);
          $("#methodModal").modal("show");
        });
      } else {
        $("#bankModal").find("#bankLocationSubmitButton").html("<img src='./images/ma.png' style='height: 60px;'>");
        $("#bankModal").find("#bankLocationSubmitButton").addClass("whiteButton");
        $('#bankModal').on('hidden.bs.modal', function () {
          $("#bankModal").find("#bankLocationSubmitButton").html("Next");
          $("#bankModal").find("#bankLocationSubmitButton").removeClass("whiteButton");
        });
        $('#methodModal').on('hidden.bs.modal', function () {
          $("#bankModal").find("#bankLocationSubmitButton").html("Next");
          $("#bankModal").find("#bankLocationSubmitButton").removeClass("whiteButton");
        });
      }
    });
  });

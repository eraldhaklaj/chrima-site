//index.js
"use strict";

// Replace your API Key here
var stripe = Stripe("pk_test_1jkmsn5VVdURNPFnxVnAJ6TV00gzh8Wlkj");
var elements = stripe.elements();

function registerElements(elements, exampleName) {
  console.log(elements, exampleName);
  var formClass = "." + exampleName;
  var example = document.querySelector(formClass);

  var form = example.querySelector("form");
  var resetButton = example.querySelector("a.reset");
  var error = form.querySelector(".error");
  var errorMessage = error.querySelector(".message");

  function enableInputs() {
    Array.prototype.forEach.call(
      form.querySelectorAll(
        "input[type='text'], input[type='email'], input[type='tel']"
      ),
      function(input) {
        input.removeAttribute("disabled");
      }
    );
  }

  // function disableInputs() {
  //     Array.prototype.forEach.call(
  //         form.querySelectorAll(
  //             "input[type='text'], input[type='email'], input[type='tel']"
  //         ),
  //         function (input) {
  //             input.setAttribute('disabled', 'true');
  //         }
  //     );
  // }

  // function triggerBrowserValidation() {
  // The only way to trigger HTML5 form validation UI is to fake a user submit
  // event.
  //     var submit = document.createElement('input');
  //     submit.type = 'submit';
  //     submit.style.display = 'none';
  //     form.appendChild(submit);
  //     submit.click();
  //     submit.remove();
  // }

  // Listen for errors from each Element, and show error messages in the UI.
  var savedErrors = {};
  elements.forEach(function(element, idx) {
    element.on("change", function(event) {
      if (event.error) {
        error.classList.remove("invisible");
        error.classList.add("visible");
        savedErrors[idx] = event.error.message;
        errorMessage.innerText = event.error.message;
      } else {
        savedErrors[idx] = null;

        // Loop over the saved errors and find the first one, if any.
        var nextError = Object.keys(savedErrors)
          .sort()
          .reduce(function(maybeFoundError, key) {
            return maybeFoundError || savedErrors[key];
          }, null);

        if (nextError) {
          // Now that they've fixed the current error, show another one.
          errorMessage.innerText = nextError;
        } else {
          // The user fixed the last error; no more errors.
          error.classList.remove("visible");
          error.classList.add("invisible");
        }
      }
    });
  });

  // Listen on the form's 'submit' handler...
  form.addEventListener("submit", function(e) {
    e.preventDefault();

    // Trigger HTML5 validation UI on the form if any of the inputs fail
    // validation.
    var plainInputsValid = true;
    Array.prototype.forEach.call(form.querySelectorAll("input"), function(
      input
    ) {
      if (input.checkValidity && !input.checkValidity()) {
        plainInputsValid = false;
        return;
      }
    });
    if (!plainInputsValid) {
      triggerBrowserValidation();
      return;
    }

    // Show a loading screen...
    example.classList.add("submitting");

    // Disable all inputs.
    // disableInputs();

    // Gather additional customer data we may have collected in our form.
    var name = document.querySelector("#full_name").value;
    var address1 = document.querySelector("#autocomplete").value;
    var city = form.querySelector("#" + exampleName + "-city");
    var state = form.querySelector("#" + exampleName + "-state");
    var zip = form.querySelector("#" + exampleName + "-zip");
    // var routingNumber = form.querySelector('#' + exampleName + '-routingNumber');
    var additionalData = {
      name: name ? name.value : undefined,
      address_line1: address1 ? address1.value : undefined,
      address_city: city ? city.value : undefined,
      address_state: state ? state.value : undefined,
      address_zip: zip ? zip.value : undefined
      // routingNumber: routingNumber ? routingNumber.value : undefined,
    };

    // Use Stripe.js to create a token. We only need to pass in one Element
    // from the Element group in order to create a token. We can also pass
    // in the additional customer data we collected in our form.
    stripe.createToken(elements[0], additionalData).then(function(result) {
      // Stop loading!
      example.classList.remove("submitting");

      if (result.token) {
        // If we received a token, show the token ID.
        stripeTokenHandler(result.token);
      } else {
        // Otherwise, un-disable inputs.
        errorMessage.textContent = result.error.message;
      }
    });

    function stripeTokenHandler(token) {
      // Insert the token ID into the form so it gets submitted to the server
      var form = document.getElementById("card-payment-form");
      form.reset();
      var hiddenInput = document.createElement("input");
      hiddenInput.setAttribute("type", "hidden");
      hiddenInput.setAttribute("name", "stripeToken");
      hiddenInput.setAttribute("value", token.id);
      form.appendChild(hiddenInput);

      var chargeInput = document.createElement("input");
      chargeInput.setAttribute("type", "hidden");
      chargeInput.setAttribute("name", "amount");
      chargeInput.setAttribute("value", donationAmount);

      form.appendChild(chargeInput);
      // Submit the form
      // form.submit();

      const data = new URLSearchParams();
      for (const pair of new FormData(form)) {
        data.append(pair[0], pair[1]);
      }
      fetch("charge.php", {
        method: "POST",
        body: data
      }).then(res => {
        console.log(res);
        form.reset();
        elements.forEach(function(element) {
          element.clear();
        });
        error.classList.add("invisible");
        if (res.status !== 200) {
          $("#cardModal").modal("hide");
          $("#cardDeclinedModal").modal("show");
          document.getElementById("cardDeclineError").innerHTML = res.message;
        } else {
          $("#cardModal").find("[data-tid='elements_examples.form.pay_button']").html("<i class='far fa-check-circle'></i>");
          $("#cardModal").find("[data-tid='elements_examples.form.pay_button']").addClass("whiteButton");
          $('#cardModal').on('hidden.bs.modal', function () {
            $("#cardModal").find("[data-tid='elements_examples.form.pay_button']").html("Donate");
            $("#cardModal").find("[data-tid='elements_examples.form.pay_button']").removeClass("blueButton");
          });
          $('#methodModal').on('hidden.bs.modal', function () {
            $("#cardModal").find("[data-tid='elements_examples.form.pay_button']").html("Donate");
            $("#cardModal").find("[data-tid='elements_examples.form.pay_button']").removeClass("blueButton");
          });
        }
      });
    }
  });
}

//exapmle.2
(function() {
  "use strict";

  var elements = stripe.elements({
    fonts: [
      {
        cssSrc: "https://fonts.googleapis.com/css?family=Poppins"
      }
      // {
      //     family: 'Sofia Pro',
      //     src: 'url(fonts/Sofia-Pro-Medium-_2_.woff) format("woff")',
      // }
    ],
    // Stripe's examples are localized to specific languages, but if
    // you wish to have Elements automatically detect your user's locale,
    // use `locale: 'auto'` instead.
    locale: window.__exampleLocale
  });

  var elementStyles = {
    base: {
      color: "#32325D",
      fontWeight: 900,
      fontFamily: "Poppins",
      fontSize: "16px",
      fontSmoothing: "antialiased",
      letterSpacing: "2px",
      "::placeholder": {
        color: "#848C92",
        fontSize: "16px",
        fontWeight: 900
      },
      ":-webkit-autofill": {
        color: "#e39f48"
      }
    },
    invalid: {
      color: "#E25950",

      "::placeholder": {
        color: "#FFCCA5"
      }
    }
  };

  var elementClasses = {
    focus: "focused",
    empty: "empty",
    invalid: "invalid"
  };

  var cardNumber = elements.create("cardNumber", {
    style: elementStyles,
    classes: elementClasses
  });
  cardNumber.mount("#masterCard-card-number");

  var cardExpiry = elements.create("cardExpiry", {
    style: elementStyles,
    classes: elementClasses
  });
  cardExpiry.mount("#masterCard-card-expiry");

  var cardCvc = elements.create("cardCvc", {
    style: elementStyles,
    classes: elementClasses
  });
  cardCvc.mount("#masterCard-card-cvc");

  registerElements([cardNumber, cardExpiry, cardCvc], "masterCard");
})();

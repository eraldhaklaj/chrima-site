// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyDwZdIG6rCofyO3ygvJ3VMQFK35HGea-SQ",
  authDomain: "mansurah-2764f.firebaseapp.com",
  databaseURL: "https://mansurah-2764f.firebaseio.com",
  projectId: "mansurah-2764f",
  storageBucket: "mansurah-2764f.appspot.com",
  messagingSenderId: "254104823195",
  appId: "1:254104823195:web:76bd33724d3f670c"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
var database = firebase.database();

function writeDonationData(
  donornametosave,
  donorAddresstosave,
  amounttosave,
  medium
) {
  amounttosave = +amounttosave / 100;
  firebase
    .database()
    .ref("donations")
    .push({
      donorname: donornametosave,
      donorAddress: donorAddresstosave,
      amount: amounttosave,
      medium: medium
    });
}

document
  .getElementById("paypalMethodButton")
  .addEventListener("click", function() {
    writeDonationData(
      document.querySelector("#full_name").value,
      document.querySelector("#autocompelte").value,
      donationAmount,
      "Paypal"
    );
  });

document.getElementById("bankMethod").addEventListener("click", function() {
  writeDonationData(
    document.querySelector("#full_name").value,
    document.querySelector("#autocomplete").value,
    donationAmount,
    "Bank"
  );
});

document
  .getElementById("cardMethodButton")
  .addEventListener("click", function() {
    writeDonationData(
      document.querySelector("#full_name").value,
      document.querySelector("#autocompelte").value,
      donationAmount,
      "Card"
    );
  });
